require 'spec_helper'

feature 'campaigns' do
  let(:user) { create :user }
  before { sign_in user }
  let(:another_user) { create :user, email: 'lol2@lol.com' }

  feature 'a user joins a campaign' do
    given!(:campaign) { create :campaign, user: another_user }
    given!(:character) { create :character, user: user }

    scenario do
      visit new_players_campaign_joins_path(campaign.id)

      select character.name, from: :join_character_id

      click_button :submit_character

      expect(page).to have_content('Character joined to campaign')
    end
  end

  feature 'a user leaves a campaign' do
    given!(:campaign) { create :campaign, user: another_user }
    given!(:character) { create :character, user: user, campaign: campaign }

    scenario do
      visit new_players_character_leaves_path(character.id)

      click_button :submit_character

      expect(page).to have_content('You have left the campaign')
    end
  end

  feature 'a user views a campaign' do
    given!(:campaign) { create :campaign }

    scenario do
      visit players_campaign_path(campaign.id)

      expect(page).to have_content(campaign.name)
    end
  end

  feature 'a user views all campaigns' do
    given!(:campaigns) { 3.times.map{ create :campaign , user: user} }

    scenario do
      visit players_campaigns_path

      campaigns.each do |campaign|
        expect(page).to have_link(campaign.name)
      end
    end
  end
end