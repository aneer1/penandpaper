require 'spec_helper'

feature "feats" do
  let(:user) { create :user }
  before { sign_in user }
  let(:character) { create :character, user: user }

  feature "a player views her feats" do
    given!(:feats) { 3.times.map { create :character_feat, character: character } }

    scenario do
      visit players_character_path(character)

      feats.each do |feat|
        expect(page).to have_content(feat.name)
      end
    end
  end

  feature "a player adds a feat" do
    scenario do
      visit new_players_character_feat_path(character)

      fill_in :feat_name, with: "Cleave"
      fill_in :feat_benefit, with: "Extra melee attack after dropping target"
      fill_in :feat_notes, with: "prereq: Power Attack"

      click_button :submit_feat

      expect(page).to have_content("Feat added")
    end
  end

end