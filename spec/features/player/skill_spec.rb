require 'spec_helper'

feature "skills" do
  let(:user) { create :user }
  before { sign_in user }
  let(:character) { create :character, user: user }

  feature "a player views her skills" do
    given!(:skills) { 3.times.map { create :character_skill, character: character } }

    scenario do
      visit players_character_path(character.id)

      skills.each do |skill|
        expect(page).to have_content(skill.name)
        expect(page).to have_content(skill.points)
        expect(page).to have_content(skill.notes)
      end
    end
  end

  feature "a player adds a new skill" do
    scenario do
      visit new_players_character_skill_path(character)

      fill_in :skill_name, with: "Appraise"
      fill_in :skill_points, with: 1
      fill_in :skill_notes, with: "INT, Class Skill"

      click_button :submit_skill

      expect(page).to have_content("Skill added")
    end
  end

  feature "a player edits one of her skills" do
    given!(:skill) { create :character_skill, character: character }

    scenario do
      visit edit_players_character_skill_path(character, skill)

      fill_in :skill_points, with: 6
      fill_in :skill_notes, with: "edited notes"

      click_button :submit_skill

      expect(page).to have_content("Skill updated")
    end
  end
end