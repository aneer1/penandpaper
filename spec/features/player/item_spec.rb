require 'spec_helper'

feature "items" do
  let(:user) { create :user }
  before { sign_in user }
  let(:character) { create :character, user: user }

  feature "a player views her character's inventory" do
    given!(:items) { 3.times.map { create :character_item, character: character } }

    scenario do
      visit players_character_path(character.id)
      items.each do |item|
        expect(page).to have_content(item.name)
        expect(page).to have_content(item.description)
      end
    end
  end

  feature "a player adds an item to her inventory" do
    scenario do
      visit new_players_character_item_path(character.id)

      fill_in :item_name, with: "Anwyn's Ring of Mean"
      fill_in :item_description, with: "+5 mean"

      click_button :submit_item

      expect(page).to have_content("Item added to inventory")
    end
  end

  feature "a player removes an item from her inventory" do
    given!(:item) { create :character_item, character: character }

    scenario do
      visit players_character_path(character.id)

      click_link "Drop?", match: :first

      expect(page).to have_content("Item has been destroyed")
      #TODO: Add bootbox confirm here
    end
  end

  feature "a player updates the quantity of an item in her inventory" do
    given!(:item) { create :character_item, character: character }

    scenario do
      visit edit_players_character_item_path(character.id, item.id)

      fill_in :item_update_quantity, with: 5
      fill_in :item_description, with: 'for spellcasting'

      click_button :submit_item

      expect(page).to have_content('quantity updated')
    end
  end
end