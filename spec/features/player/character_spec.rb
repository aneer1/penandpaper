require 'spec_helper'

feature 'player/characters' do
  let(:user){ create :user }
  before { sign_in user }

  feature "a player creates a new character" do
    scenario do
      visit new_players_character_path

      fill_in :character_name, with: "test"
      fill_in :character_race, with: "elf"
      fill_in :character_char_class, with: "cleric"
      fill_in :character_alignment, with:"chaotic good"
      fill_in :character_gender, with: "female"
      fill_in :character_bio, with: "testeststest"
      #end generic character info
      fill_in :character_total_str, with: 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+)
      fill_in :character_total_dex, with: 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+)
      fill_in :character_total_con, with: 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+)
      fill_in :character_total_int, with: 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+)
      fill_in :character_total_wis, with: 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+)
      fill_in :character_total_cha, with: 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+)

      click_button :submit_character

      expect(page).to have_content("Character Created!")
    end
  end

  feature "a player views her homepage" do
    given!(:chars) { 3.times.map{ create :character, user: user } }

    scenario do
      visit players_characters_path

      chars.each do |character|
        expect(page).to have_link(character.name)
      end
    end
  end

  feature "a player edits her character" do
    given!(:char) { create :character, user: user }

    scenario do
      visit edit_players_character_path(char.id)

      fill_in :character_name, with: "updated"
      fill_in :character_bio, with: "\n\nupdated"

      click_button :submit_character

      expect(page).to have_content("Character Updated!")
    end
  end

end