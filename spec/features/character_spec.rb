require 'spec_helper'

feature "characters" do

  feature "a guest visits the characters home page" do
    given!(:user){ create :user }
    given!(:chars) { 3.times.map { create :character, user: user } }

    scenario do
      visit characters_path

      expect(page).to have_content("Character Index")
      chars.each do |char|
        expect(page).to have_link(char.name)
      end
    end
  end
  feature "a guest visits a character's home page" do
    given!(:char) { create :character }

    scenario do
      visit character_path(char.id)

      expect(page).to have_content(char.name)
      expect(page).to have_content(char.bio)
    end
  end

end
