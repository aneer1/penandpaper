require 'spec_helper'

feature 'game_masters/characters' do
  let(:user) { create :user }
  before{ sign_in user }
  let(:campaign) {create :campaign, user: user }

  feature 'a game master views a character sheet' do
    given!(:character) { create :character, campaign: campaign }

    scenario do
      visit game_masters_campaign_character_path(campaign.id, character.id)

      expect(page).to have_content(character.name)
    end
  end

  feature 'a game master views all characters on a campaign' do
    given!(:user_a) { create :user, email:'ab1c@abc.com' }
    given!(:characters) { 3.times.map { create :character, campaign: campaign, user: user_a } }
    scenario do
      visit game_masters_campaign_characters_path(campaign.id)

      characters.each do |character|
        expect(page).to have_content(character.name)
      end
    end
  end

  feature 'a game master awards a character experience' do
    given!(:character) { create :character, campaign: campaign }

    scenario do
      visit edit_game_masters_campaign_character_path(campaign.id, character.id)

      fill_in :character_add_experience, with: 500
      fill_in :character_add_money, with: 1000

      click_button :submit_experience

      expect(page).to have_content('character updated')
    end
  end
end