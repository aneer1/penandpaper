require 'spec_helper'

feature 'gm/characters/items' do
  let(:user) { create :user }
  before{ sign_in user }
  let!(:campaign) { create :campaign, user: user }

  feature 'a gm gives a character an item' do
    let(:character) { create :character, campaign: campaign }

    scenario do
      visit new_game_masters_campaign_character_item_path(campaign.id, character.id)

      fill_in :item_name, with: "Anwyn's Ring of Mean"
      fill_in :item_description, with: "+5 mean"

      click_button :submit_item
      expect(page).to have_content("Item added to #{character.name}'s inventory")
    end
  end
end