require 'spec_helper'

feature 'game_master/campaign' do
  let(:user){ create :user }
  before { sign_in user }

  feature 'a game master views characters on a campaign' do
    given!(:campaign){ create :campaign, user: user }
    given!(:another_user){ create :user, email:'abec1@abe.com' }
    given!(:characters) { 3.times.map { create :character, campaign: campaign, user: another_user } }

    scenario do
      visit game_masters_campaign_path(campaign.id)

      expect(page).to have_content(campaign.name)
      expect(page).to have_content(campaign.description)

      characters.each do |character|
        expect(page).to have_content(character.name)
        expect(page).to have_content(character.class)
      end
    end
  end

  feature 'a game master views her active campaigns' do
    given!(:campaigns) { 3.times.map {create :campaign, user: user } }

    scenario do
      visit game_masters_campaigns_path

      campaigns.each do |campaign|
        expect(page).to have_content(campaign.name)
        expect(page).to have_content(campaign.description)
      end
    end

    feature 'game master creates a campaign' do
      scenario do
        visit new_game_masters_campaign_path

        fill_in :campaign_name, with: "test"
        fill_in :campaign_description, with: "testeststest"
        fill_in :campaign_meets_when, with: "Wednesdays @ 6"
        fill_in :campaign_meets_at, with: "PPF"

        click_button :submit_campaign

        expect(page).to have_content("Campaign Created!")
      end
    end

    feature 'game master edits a campaign' do
      given!(:campaign) { create :campaign, user: user }
      scenario do
        visit edit_game_masters_campaign_path(campaign.id)

        fill_in :campaign_name, with:"updated"
        fill_in :campaign_description, with: "testst"
        fill_in :campaign_meets_when, with: "Wednesdays @ 6"
        fill_in :campaign_meets_at, with: "PPF"

        click_button :submit_campaign

        expect(page).to have_content("Campaign Updated")
      end
    end
  end
end