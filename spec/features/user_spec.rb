require 'spec_helper'

feature 'user' do
  feature "a user views a profile account" do
    let(:user) { create :user }
    before { sign_in user }

    scenario do
      visit players_pages_path(user.id)

      expect(page).to have_content(user.user_name)
    end
  end

  feature "a new user signs up" do
    #TODO: ask james how to do a mailer for this.
    scenario do
      visit new_users_sign_ups_path

      fill_in :user_email, with: "thalajyrk@alineer.com"
      fill_in :user_user_name, with: "thalajyrk"
      fill_in :user_password, with: "password1"
      fill_in :user_password_confirmation, with:"password1"
      fill_in :user_about, with:"I'm an awesome GM!"

      click_button :submit_user

      expect(page).to have_content("Thank you for signing up")
    end
  end

  feature "a user updates her information" do
    let(:user) { create :user }
    before { sign_in user }

    scenario do
      visit edit_users_profiles_path(user.id)

      fill_in :user_email, with: 'random@alineer.com'
      fill_in :user_password, with: "lollollollollollol"
      fill_in :user_password_confirmation, with: "lollollollollollol"
      fill_in :user_about, with:"I'm an awesome GM!"

      click_button :submit_user

      expect(page).to have_content("Profile Updated")
    end
  end

  feature "a user views an another user's account" do
    let(:user) { create :user }
    before { sign_in user }
    given!(:another_user){ create :user, email:'abc1@abc.com'}

    scenario do
      visit user_path(another_user)

      expect(page).to have_content(another_user.user_name)
    end
  end
end