require 'spec_helper'

feature 'campaigns' do

  feature 'a guest views all campaigns' do
    given!(:user) { create :user }
    given!(:campaigns) { 3.times.map { create :campaign, user: user } }

    scenario do
      visit campaigns_path

      campaigns.each do |campaign|
        expect(page).to have_content(campaign.name)
      end
    end
  end

  feature 'a guest views a single campaign' do
    given!(:campaign) { create :campaign }

    scenario do
      visit campaign_path(campaign.id)

      expect(page).to have_content(campaign.name)
      expect(page).to have_content(campaign.description)
    end
  end
end