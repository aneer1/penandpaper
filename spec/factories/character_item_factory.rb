FactoryGirl.define do
  factory :character_item, class: Character::Item do
    sequence(:name){ |n| "Thal's staff of pedantry ##{n}" }
    sequence(:description){|n| "+#{n} pedantry"}
  end
end