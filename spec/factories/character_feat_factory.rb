FactoryGirl.define do
  factory :character_feat, class: Character::Feat do
    sequence(:name){ |n| "Feat#{n}" }
    sequence(:benefit){|n| "+#{n} benefit"}
  end
end