FactoryGirl.define do
  factory :user do
    email { "abc@abc.com" }
    password { 'lollollol' }
    password_confirmation { 'lollollol' }
    user_name {|n| "user#{n}" }
    about { "this is a test" }
  end
end