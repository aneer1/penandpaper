FactoryGirl.define do
  factory :character do
    sequence(:name) {|n| "Character#{n}" }
    sequence(:bio){"This is a quick bio" }
    sequence(:race){ "elf" }
    sequence(:char_class){ "cleric" }
    sequence(:alignment){"chaotic good"}
    sequence(:gender){"female"}

    sequence(:total_str) { 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+) } #fun fact these were chosen in true dnd dice roll fashion!
    sequence(:total_dex) { 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+) }
    sequence(:total_con) { 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+) }
    sequence(:total_int) { 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+) }
    sequence(:total_wis) { 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+) }
    sequence(:total_cha) { 4.times.map { SecureRandom.random_number(6) + 1 }.sort.last(3).inject(&:+) }
  end
end