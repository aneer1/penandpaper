FactoryGirl.define do
  factory :character_skill, class: Character::Skill do
    sequence(:name) { |n| "skill#{n}" }
    sequence(:points) { |n| n }
    sequence(:notes){ |n| "note#{n}" }
  end
end