FactoryGirl.define do
  factory :campaign do
    sequence(:name) { |n| "Campaign#{n}" }
    sequence(:description) { "This is a description" }
    sequence(:meets_when) { "Friday at 5" }
    sequence(:meets_at) { "that comic shop" }
  end
end