require 'spec_helper'

describe Players::Campaigns::JoinsController do
  let(:user){ create :user }
  before{ sign_in user }
  let(:character){ create :character, user: user }

  context 'with a campaign owned by a different user' do
    let(:campaign) { create :campaign }

    context 'with no characters already on that campaign' do
      describe '#create' do
        it 'allows the character to join' do
          post :create, campaign_id: campaign.id, join: { character_id: character.id }

          expect(response).to redirect_to([:players, campaign])
        end
      end
    end

    context 'with a character already on that campaign' do
      let!(:joined_character) { create :character, user: user, campaign: campaign}

      describe '#create' do
        it 'does not allow the character to join' do
          post :create, campaign_id: campaign.id, join: { character_id: character.id }

          expect(response).to render_template(:new)
        end
      end
    end
  end

  context 'with a campaign owned by the user owning the character' do
    let(:campaign) { create :campaign, user: user }
    describe "#create" do
      it 'does not allow the character to join' do
        post :create, campaign_id: campaign.id, join: { character_id: character.id }

        expect(response).to render_template(:new)
      end
    end
  end

  context 'with a character owned by someone else' do
    let(:campaign){ create :campaign }
    let!(:different_character){ create :character }
    describe "#create" do
      it 'does not allow the character to join' do
        post :create, campaign_id: campaign.id, join: { character_id: different_character.id}

        expect(response).to render_template(:new)
      end
    end
  end

  context 'with an inactive campaign' do
    let(:campaign){ create :campaign, status: 2 }
    describe "#create" do
      it "does not allow the character to join " do
        post :create, campaign_id: campaign.id, join: { character_id: character }

        expect(response).to render_template(:new)
      end
    end
  end
end
