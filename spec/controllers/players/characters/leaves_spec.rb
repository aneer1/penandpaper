require 'spec_helper'

describe Players::Characters::LeavesController do
  let(:user) { create :user }
  before { sign_in user }

  context 'character is owned by the user and on a campaign' do
    let(:campaign) { create :campaign }
    let(:character) { create :character, user: user, campaign: campaign }

    describe '#create' do
      it 'allows the character to leave the campaign' do
        post :create, character_id: character.id

        expect(response).to redirect_to([:players, character])
      end
    end
  end
  context 'character is not on a campaign' do
    let(:campaign) { create :campaign }
    let(:character) { create :character, user: user }

    describe '#create' do
      it 'does not allow character to leave the campaign' do
        post :create, character_id: character.id

        expect(response).to render_template(:new)
      end
    end
  end
end