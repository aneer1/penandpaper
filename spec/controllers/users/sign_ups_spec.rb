require 'spec_helper'

describe Users::SignUpsController do

  context 'without a logged in user' do
    describe "#new" do
      it "assigns the sign-up form" do
        get :new

        expect(render_template(:new)).to be_present
      end
    end

    describe "#create" do
      context 'when email and password are valid' do
        it 'saves the user' do
          post :create, user: attributes_for(:user)

          expect(session[:user_id]).to eq(User.last.id)
        end

        it 'redirects to players page' do
          post :create, user: attributes_for(:user)

          expect(response).to redirect_to([:players, :pages])
        end
      end

      context 'when email matches another user' do
        let!(:user){ create :user }

        it 'renders #new' do
          post :create, user: attributes_for(:user)

          expect(response).to render_template(:new)
        end
      end

      context 'passwords do not match' do
        it 'renders #new' do
          post :create, user: attributes_for(:user, password_confirmation: "abc12345")

          expect(response).to render_template(:new)
        end
      end

      context 'password is too short' do
        it 'renders #new' do
          post :create, user: attributes_for(:user, password:'abc', password_confirmation:'abc')

          expect(response).to render_template(:new)
        end
      end

      context "user name matches another user's" do
        let!(:user){ create :user}

        it 'renders #new' do
          post :create, user: attributes_for(:user, email:'abc@abc.com')

          expect(response).to render_template(:new)
        end
      end
    end
  end

  context 'with a logged in user' do
    let(:user){ create :user }
    before{ sign_in(user) }

    describe '#new' do
      it 'renders 404' do
        get :new

        expect(response.response_code).to eq(404)
      end
    end
  end
end