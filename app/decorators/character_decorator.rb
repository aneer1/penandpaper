class CharacterDecorator < Draper::Decorator
  delegate_all

  def render_title
    h.capture do
      h.content_tag :h2, class: 'page-header' do
        h.concat name
        h.concat " "
        h.concat title_small
        if block_given?
          h.concat h.capture { yield }
        end
      end
    end
  end

  def render_bio
    h.simple_format(bio)
  end

  def link_name(*scope)
    h.link_to name, [*scope, object]
  end

  def render_edit_link
    h.link_to(h.content_tag(:i, '', class: 'fa fa-pencil'), h.polymorphic_path([:edit, :players, object]), class:'btn btn-success btn-xs')
  end

  private
  def title_small
    h.content_tag :small do
      "the level #{level} #{race} #{char_class}"
    end
  end
end