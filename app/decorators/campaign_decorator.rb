class CampaignDecorator < Draper::Decorator
  delegate_all

  decorates_association :characters

  def render_header
    h.capture do
      h.content_tag :h2, class: 'page-header' do
        h.concat name
      end
    end
  end

  def render_description
    h.simple_format(description)
  end

  def link_name (*scope)
    h.link_to name, [*scope, object]
  end
end