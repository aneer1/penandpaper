class UserDecorator < Draper::Decorator
  delegate_all

  decorates_association :characters

  def edit_link
    h.link_to "Update Information", [:edit, :users, :profiles]
  end
end