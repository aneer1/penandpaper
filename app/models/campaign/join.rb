class Campaign
  class Join < ActiveType::Object
    attribute :character_id, :integer
    attribute :campaign_id, :integer
    attribute :user_id, :integer

    belongs_to :character
    belongs_to :campaign
    belongs_to :user

    validate :validate_character_belongs_to_user
    validate :validate_no_characters_in_campaign
    validate :validate_user_does_not_own_campaign
    validate :validate_campaign_is_active

    after_save :assign_campaign

    private
    def validate_character_belongs_to_user
      unless user.characters.include?(character)
        errors[:character] << 'must belong to user'
      end
    end

    def validate_no_characters_in_campaign
      if user.characters.in_campaign(campaign).present?
        errors[:character] << 'only one character allowed per campaign'
      end
    end

    def validate_user_does_not_own_campaign
      if user.campaigns.include?(campaign)
        errors[:character] << 'cannot join a campaign owned by yourself.'
      end
    end

    def validate_campaign_is_active
      unless Campaign.is_active.include?(campaign) #TODO: Ask James if there's a better way, probably is.
        errors[:character] << 'cannot join an inactive campaign'
      end
    end

    def assign_campaign
      character.update_attributes campaign: campaign
    end
  end
end