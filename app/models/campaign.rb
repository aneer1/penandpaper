class Campaign < ActiveRecord::Base
  extend Enumerize

  validates :name, presence: true
  validates :description, presence: true

  validates :meets_at, presence: true
  validates :meets_when, presence: true

  belongs_to :user
  has_many :characters

  enumerize :status, in: { active: 1, inactive: 2 }, default: :active, scope: true
  scope :active, -> { with_status :active }

  def self.use_relative_model_naming?
    true
  end

  def user_name
    user.user_name
  end

  def self.is_active
    where(status: 1)
  end
end