class Character < ActiveType::Record
  attribute :add_experience, :integer
  validates :add_experience, numericality: { greater_than: 0, allow_nil: true }

  attribute :add_money, :integer
  validates :add_money, numericality: { allow_nil: true }

  attribute :gold, :integer
  validates :add_money, numericality: { allow_nil: true }
  attribute :silver, :integer
  validates :add_money, numericality: { allow_nil: true }
  attribute :copper, :integer
  validates :add_money, numericality: { allow_nil: true }

  validates :name, presence:  true
  validate :bio, presence:  true
  validates :race, presence: true
  validates :char_class, presence: true
  validates :alignment, presence: true
  validates :gender, presence: true

  validates :total_str, presence: true
  validates :total_dex, presence: true
  validates :total_con, presence: true
  validates :total_int, presence: true
  validates :total_wis, presence: true
  validates :total_cha, presence: true

  belongs_to :user
  belongs_to :campaign

  has_many :items
  has_many :skills
  has_many :feats

  scope :not_in_campaign, -> { where(campaign: nil) }
  scope :in_any_campagin, -> { where.not(campaign: nil) }

  before_save :assign_experience
  before_save :update_level
  before_save :assign_money

  def gold_silver_copper(money)
    #copper:
    #G = 1/100
    #S = 1/10
    #C = 1
    self.gold = money / 100
    money = money - self.gold * 100
    self.silver = money / 10
    money = money - self.silver * 10
    self.copper = money % 10
  end

  def level_progress(experience, level)
    to_level = (level + 1) * level * 500
    remaining = to_level - (to_level - experience)
    ((remaining.to_f / to_level) * 100).round(2)
  end

  def user_name
    user.user_name
  end

  def self.use_relative_model_naming?
    true
  end

  def self.in_campaign(campaign)
    where(campaign: campaign)
  end

  def campaign_name
    campaign.name
  end

  private
  def experience_into_level
    #This formula (thanks to David Richards) is even simpler: level = floor[(1+sqrt(XP/125 + 1))/2]
    #curtesy of http://www.monkeysushi.net/gaming/DnD/math.html
    return ((1 + Math.sqrt((experience / 125) + 1 )) / 2).floor
  end

  def assign_experience
    return unless add_experience.present?

    self.experience += add_experience
    self.add_experience = nil
  end

  def update_level
    return unless experience_changed?

    self.level = experience_into_level
  end

  def assign_money #TODO: make this presentable. break up copper into P/G/S/C
    return unless add_money.present?

    self.money += add_money
    self.add_money = nil
  end
end