class User < ActiveRecord::Base
  validates :email, presence: true, uniqueness: true

  has_secure_password
  validates :password_confirmation, presence: true, if: lambda { !password.nil? }, length: { minimum: 8 }

  validates :user_name, presence: true, uniqueness: true

  has_many :characters
  has_many :campaigns

  def email=(value)
    super(value.downcase)
  end
end