class Character
  class Feat < ActiveRecord::Base
    validates :name, presence: true
    validates :benefit, presence: true

    belongs_to :character
  end
end
