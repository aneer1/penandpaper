class Character
  class Item < ActiveType::Record
    attribute :update_quantity, :integer
    validates :update_quantity, numericality: { allow_nil: true }

    validates :name, presence: true
    validates :description, presence: true
    validates :quantity, numericality: { greater_than_or_equal_to: 0 }

    belongs_to :character

    before_save :assign_quantity

    private
    def assign_quantity
      return unless update_quantity.present?

      self.quantity = update_quantity
      self.update_quantity = 0
    end
  end
end