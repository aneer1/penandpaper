class Character
  class Leave < ActiveType::Object
    attribute :character_id, :integer
    belongs_to :character

    validate :validate_character_on_campaign
    after_save :unassign_campaign

    private
    def validate_character_on_campaign
      unless character.campaign.present?
        errors[:campaign] << 'character is not adventuring right now'
      end
    end

    def unassign_campaign
      character.update_attributes campaign: nil
    end
  end
end