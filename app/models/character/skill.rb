class Character
  class Skill < ActiveRecord::Base
    validates :name, presence: true
    validates :points, presence: true

    belongs_to :character
  end
end