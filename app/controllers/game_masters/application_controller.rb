module GameMasters
  class ApplicationController < ::ApplicationController
    before_action :ensure_sign_in

    private
    def ensure_sign_in
      redirect_to [:new, :sessions] unless current_user.present?
    end
  end
end