module GameMasters
  class CampaignsController < ::GameMasters::ApplicationController
    decorates_assigned :campaign, :campaigns

    def index
      @campaigns = current_user.campaigns.all
      @campaigns = current_user.campaigns.active.all unless params[:show_all].present?
    end

    def show
      @campaign = current_user.campaigns.includes(:characters).find(params[:id])
    end

    def new
      @campaign = current_user.campaigns.build
    end

    def create
      @campaign = current_user.campaigns.build(campaign_params)
      if @campaign.save
        flash[:notice] = 'Campaign Created!'
        redirect_to @campaign
      else
        render :new
      end
    end

    def edit
      @campaign = current_user.campaigns.find(params[:id])
    end

    def update
      @campaign = current_user.campaigns.find(params[:id])
      if @campaign.update_attributes(campaign_params)
        flash[:notice] = "Campaign Updated"
        redirect_to @campaign
      else
        render :edit
      end
    end

    private
    def campaign_params
      params.require(:campaign).permit(:name, :description, :meets_at, :meets_when, :status)
    end
  end
end