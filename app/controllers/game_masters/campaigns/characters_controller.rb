module GameMasters
  module Campaigns
    class CharactersController < ::GameMasters::Campaigns::ApplicationController
      decorates_assigned :character, :characters

      def index
        @characters = @campaign.characters
      end

      def show
        @character = @campaign.characters.find(params[:id])
      end

      def edit
        @character = @campaign.characters.find(params[:id])
      end

      def update
        @character = @campaign.characters.find(params[:id])

        if @character.update_attributes(character_params)
          flash[:notice] = 'character updated'
          redirect_to [:game_masters, @campaign, @character]
        else
          render :edit
        end
      end

      private
      def character_params
        params.require(:character).permit(:add_experience, :add_money)
      end
    end
  end
end
