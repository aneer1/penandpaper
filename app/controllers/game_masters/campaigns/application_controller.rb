module GameMasters
  module Campaigns
    class ApplicationController < ::GameMasters::ApplicationController
      before_action :load_campaign

      private
      def load_campaign
        @campaign = current_user.campaigns.find(params[:campaign_id])
      end
    end
  end
end