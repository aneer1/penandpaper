module GameMasters
  module Campaigns
    module Characters
      class ItemsController < ::GameMasters::Campaigns::Characters::ApplicationController
        def new
          @item = @character.items.build
        end

        def create
          @item = @character.items.build(item_params)
          if @item.save
            flash[:notice] = "Item added to #{@character.name}'s inventory"
            redirect_to [:game_masters, @campaign, @character]
          else
            render :new
          end
        end

        private
        def item_params
          params.require(:item).permit(:name, :description, :update_quantity)
        end
      end
    end
  end
end