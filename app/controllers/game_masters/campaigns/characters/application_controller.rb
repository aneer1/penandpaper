module GameMasters
  module Campaigns
    module Characters
      class ApplicationController < ::GameMasters::Campaigns::ApplicationController
        before_action :load_character

        private
        def load_character
          @character = @campaign.characters.find(params[:character_id])
        end
      end
    end
  end
end
