class CampaignsController < ApplicationController
  decorates_assigned :campaign, :campaigns

  def index
    @campaigns = Campaign.active.all
  end

  def show
    @campaign = Campaign.find(params[:id])
  end
end