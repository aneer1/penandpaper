module Players
  module Campaigns
    class JoinsController < ::Players::Campaigns::ApplicationController
      before_action :load_characters

      def new
        @join = Campaign::Join.new(campaign: @campaign, user: current_user)
      end

      def create
        @join = Campaign::Join.new(join_params.merge(campaign: @campaign, user: current_user))

        if @join.save
          flash[:notice] = 'Character joined to campaign'
          redirect_to [:players, @campaign]
        else
          render :new
        end
      end

      private
      def join_params
        params.require(:join).permit(:character_id)
      end

      def load_characters
        @characters = current_user.characters.not_in_campaign
      end
    end
  end
end