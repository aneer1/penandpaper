module Players
  module Campaigns
    class ApplicationController < ::Players::ApplicationController
      before_action :load_campaign

      private
      def load_campaign
        @campaign = Campaign.find(params[:campaign_id])
      end
    end
  end
end