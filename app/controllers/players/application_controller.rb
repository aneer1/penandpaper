module Players
  class ApplicationController < ::ApplicationController
    before_filter :ensure_sign_in

    private
    def ensure_sign_in
      redirect_to [:new, :sessions] unless current_user.present?
    end
  end
end