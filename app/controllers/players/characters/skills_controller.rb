module Players
  module Characters
    class SkillsController < ::Players::Characters::ApplicationController
      def new
        @skill = @character.skills.build
      end

      def create
        @skill = @character.skills.build(create_skill_params)
        if @skill.save
          flash[:notice] = "Skill added"
          redirect_to [:players, @character]
        else
          render :new
        end
      end

      def edit
        @skill = @character.skills.find(params[:id])
      end

      def update
        @skill = @character.skills.find(params[:id])
        if @skill.update_attributes(update_skill_params)
          flash[:notice] = "Skill updated"
          redirect_to [:players, @character]
        else
          render :edit
        end
      end

      private
      def create_skill_params
        params.require(:skill).permit(:name, :points, :notes)
      end

      def update_skill_params
        params.require(:skill).permit(:points, :notes)
      end

    end
  end
end
