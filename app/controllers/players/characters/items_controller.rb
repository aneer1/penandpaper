module Players
  module Characters
    class ItemsController < ::Players::Characters::ApplicationController
      def new
        @item = @character.items.build
      end

      def create
        @item = @character.items.build(item_params)
        if @item.save
          flash[:notice] = "Item added to inventory"
          redirect_to [:players, @character]
        else
          render :new
        end
      end

      def edit
        @item = @character.items.find(params[:id])
      end

      def update
        @item = @character.items.find(params[:id])
        if @item.update_attributes(item_params)
          flash[:notice] = 'quantity updated'
          redirect_to [:players, @character]
        else
          render :edit
        end
      end

      def destroy
        @task = @character.items.find(params[:id]).destroy
        flash[:notice] = "Item has been destroyed"
        redirect_to [:players, @character]
      end

      private
      def item_params
        params.require(:item).permit(:name, :description, :update_quantity)
      end
    end
  end
end