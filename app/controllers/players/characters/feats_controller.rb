module Players
  module Characters
    class FeatsController < ::Players::Characters::ApplicationController

      def new
        @feat = @character.feats.build
      end

      def create
        @feat = @character.feats.build(feat_params)
        if @feat.save
          flash[:notice] = "Feat added"
          redirect_to [:players, @character]
        else
          render :new
        end
      end

      private
      def feat_params
        params.require(:feat).permit(:name, :benefit, :notes)
      end
    end
  end
end
