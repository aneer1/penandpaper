module Players
  module Characters
    class ApplicationController < ::Players::ApplicationController
      before_action :load_character

      private
      def load_character
        @character = current_user.characters.find(params[:character_id])
      end
    end
  end
end