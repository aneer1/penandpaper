module Players
  module Characters
    class LeavesController < ::Players::Characters::ApplicationController

      def new
        @leave = Character::Leave.new(character: @character)
      end

      def create
        @leave = Character::Leave.new(character: @character)
        if @leave.save
          flash[:notice] = 'You have left the campaign'
          redirect_to [:players, @character]
        else
          render :new
        end
      end
    end
  end
end