module Players
  class PagesController < ::Players::ApplicationController
    decorates_assigned :user, :characters, :campaigns

    def show
      @user = current_user
      @characters = @user.characters.includes(:campaign)
      @campaigns = @user.campaigns
    end
  end
end