module Players
  class CharactersController < ::Players::ApplicationController
    decorates_assigned :character, :characters

    def index
      @characters = current_user.characters.includes(:campaign)
    end

    def show
      @character = current_user.characters.find(params[:id])
    end

    def new
      @character = Character.new
    end

    def create
      @character = Character.new(character_params.merge(user: current_user))

      if @character.save
        flash[:notice] = "Character Created!"
        redirect_to [:players, @character]
      else
        render :new
      end
    end

    def edit
      @character = current_user.characters.find(params[:id])
    end

    def update
      @character = current_user.characters.find(params[:id])

      if @character.update_attributes(character_params)
        flash[:notice] =  "Character Updated!"
        redirect_to [:players, @character]
      else
        render :edit
      end

    end

    private
    def character_params
      params.require(:character).permit(:name, :bio, :race, :char_class, :alignment, :gender, :total_str,:total_dex,:total_con, :total_int, :total_wis, :total_cha)
    end
  end
end