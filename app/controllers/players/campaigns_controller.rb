module Players
  class CampaignsController < ::Players::ApplicationController
    decorates_assigned :campaign, :campaigns
    def index
      @campaigns = Campaign.active.all.includes(:user)
    end

    def show
      @campaign = Campaign.find(params[:id])
    end
  end
end