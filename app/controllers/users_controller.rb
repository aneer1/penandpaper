class UsersController < ApplicationController
  decorates_assigned :user, :users

  def show
    @user = User.find(params[:id])
  end
end