module Users
  class ProfilesController < ::ApplicationController
    decorates_assigned :user

    def edit
      @user = current_user
    end

    def update
      @user = current_user

      if @user.update(user_params)
        flash[:notice] = "Profile Updated"
        redirect_to [:players, :pages]
      else
        render :edit
      end
    end

    private
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :user_name, :about)
    end
  end
end