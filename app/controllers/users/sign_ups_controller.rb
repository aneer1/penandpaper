module Users
  class SignUpsController < ::ApplicationController
    before_action :ensure_anonymous

    def new
      @user = User.new
    end

    def create
      @user = User.new(user_params)

      if @user.save
        flash[:notice] = "Thank you for signing up"
        self.current_user= @user
        redirect_to [:players, :pages]
      else
        render :new
      end
    end

    private
    def ensure_anonymous
      render_404 if current_user.present?
    end
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :user_name, :about)
    end
  end
end