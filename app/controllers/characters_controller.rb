class CharactersController < ApplicationController
  decorates_assigned :character, :characters

  def index
    @characters = Character.all.includes(:user, :campaign)
  end

  def show
    @character = Character.find(params[:id])
  end
end