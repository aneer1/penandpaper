class AddDefaultToLevel < ActiveRecord::Migration
  def change
    change_column :characters, :level, :integer, default: 1
  end
end
