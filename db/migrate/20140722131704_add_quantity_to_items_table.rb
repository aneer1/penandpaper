class AddQuantityToItemsTable < ActiveRecord::Migration
  def change
    add_column :character_items, :quantity, :integer, default: 0
  end
end
