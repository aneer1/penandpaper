class AddCharIdToCharSheet < ActiveRecord::Migration
  def change
    add_column :character_sheets, :character_id, :integer
  end
end
