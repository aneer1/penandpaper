class CreateFeat < ActiveRecord::Migration
  def change
    create_table :feats do |t|
      t.string :name
      t.text :benefit
      t.text :notes
      t.integer :character_id
    end
  end
end
