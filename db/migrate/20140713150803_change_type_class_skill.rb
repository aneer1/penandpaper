class ChangeTypeClassSkill < ActiveRecord::Migration
  def change
    remove_column :character_skills, :class_skill
    add_column :character_skills, :class_skill, :boolean
  end
end
