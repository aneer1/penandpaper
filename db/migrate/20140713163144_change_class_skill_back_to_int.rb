class ChangeClassSkillBackToInt < ActiveRecord::Migration
  def change
    remove_column :character_skills, :class_skill

    add_column :character_skills, :class_skill, :integer
  end
end
