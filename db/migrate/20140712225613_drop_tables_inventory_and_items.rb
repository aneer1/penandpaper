class DropTablesInventoryAndItems < ActiveRecord::Migration
  def change
    drop_table :inventories
    drop_table :items
  end
end
