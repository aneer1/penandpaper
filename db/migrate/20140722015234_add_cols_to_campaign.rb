class AddColsToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :meets_at, :string
    add_column :campaigns, :meets_when, :string
  end
end
