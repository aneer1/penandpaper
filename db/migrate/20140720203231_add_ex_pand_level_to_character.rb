class AddExPandLevelToCharacter < ActiveRecord::Migration
  def change
    add_column :characters, :level, :integer
    add_column :characters, :experience, :integer
  end
end
