class RenameItemsToCharacterItems < ActiveRecord::Migration
  def change
    rename_table :items, :character_items
  end
end
