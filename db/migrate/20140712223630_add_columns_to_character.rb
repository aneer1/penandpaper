class AddColumnsToCharacter < ActiveRecord::Migration
  def change
    drop_table :character_sheets
    add_column :characters, :total_str, :integer
    add_column :characters, :total_dex, :integer
    add_column :characters, :total_con, :integer
    add_column :characters, :total_int, :integer
    add_column :characters, :total_wis, :integer
    add_column :characters, :total_cha, :integer
  end
end
