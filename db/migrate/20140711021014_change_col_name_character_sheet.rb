class ChangeColNameCharacterSheet < ActiveRecord::Migration
  def change
    rename_column :character_sheets, :class, :char_class
  end
end
