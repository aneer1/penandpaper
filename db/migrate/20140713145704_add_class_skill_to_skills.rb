class AddClassSkillToSkills < ActiveRecord::Migration
  def change
    add_column :character_skills, :class_skill, :integer
  end
end
