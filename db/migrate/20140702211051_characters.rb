class Characters < ActiveRecord::Migration
  def change
    create_table "characters" do |t|
    t.string "name"
    t.text "bio"
    t.datetime "created_at"
    t.datetime "updated_at"
      end
  end
end
