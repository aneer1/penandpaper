class ChangeExpColumnToHaveDefault < ActiveRecord::Migration
  def change
    change_column :characters, :experience, :integer, default: 0
  end
end
