class RefactorMigration < ActiveRecord::Migration
  def change
    remove_column :character_sheets, :race, :string
    remove_column :character_sheets, :char_class, :string
    remove_column :character_sheets, :alignment, :string
    add_column :character_sheets, :total_str, :integer
    add_column :character_sheets, :total_dex, :integer
    add_column :character_sheets, :total_con, :integer
    add_column :character_sheets, :total_int, :integer
    add_column :character_sheets, :total_wis, :integer
    add_column :character_sheets, :total_cha, :integer

    add_column :characters, :gender, :string
  end
end
