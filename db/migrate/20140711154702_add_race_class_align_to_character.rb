class AddRaceClassAlignToCharacter < ActiveRecord::Migration
  def change
    add_column :characters, :race, :string
    add_column :characters, :char_class, :string
    add_column :characters, :alignment, :string
  end
end
