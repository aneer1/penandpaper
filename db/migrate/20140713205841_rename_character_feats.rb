class RenameCharacterFeats < ActiveRecord::Migration
  def change
    rename_table :feats, :character_feats
  end
end
