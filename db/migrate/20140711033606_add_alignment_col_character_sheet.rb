class AddAlignmentColCharacterSheet < ActiveRecord::Migration
  def change
    add_column :character_sheets, :alignment, :string
  end
end
