class ChangeItBack < ActiveRecord::Migration
  def change
    change_column :characters, :xp_into_level, :integer
  end
end
