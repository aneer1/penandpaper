class CreateInventoryTable < ActiveRecord::Migration
  def change
    create_table :inventory do |t|
      t.integer :character_sheet_id
      t.timestamps :created_at
    end
    create_table :items do |t|
      t.string :name
      t.string :description
      t.integer :inventory_id
    end
  end
end
