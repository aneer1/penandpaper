class RenamePlayerIdToUserId < ActiveRecord::Migration
  def change
    rename_column :characters, :player_id, :user_id
  end
end
