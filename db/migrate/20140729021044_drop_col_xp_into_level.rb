class DropColXpIntoLevel < ActiveRecord::Migration
  def change
    remove_column :characters, :xp_into_level, :integer
  end
end
