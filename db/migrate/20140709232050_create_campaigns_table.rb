class CreateCampaignsTable < ActiveRecord::Migration
  def change
    create_table "campaigns" do |t|
      t.string "name"
      t.text "description"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end
end
