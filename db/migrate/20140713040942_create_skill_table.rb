class CreateSkillTable < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.string :name
      t.integer :points
      t.integer :character_id
    end
  end
end
