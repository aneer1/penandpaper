class AddDefaultToMoney < ActiveRecord::Migration
  def change
    change_column :characters, :money, :integer, default: 0
  end
end
