class ChangeXpIntoLevelToFloat < ActiveRecord::Migration
  def change
    change_column :characters, :xp_into_level, :float
  end
end
