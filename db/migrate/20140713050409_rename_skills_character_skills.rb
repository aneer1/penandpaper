class RenameSkillsCharacterSkills < ActiveRecord::Migration
  def change
    rename_table :skills, :character_skills
  end
end
