# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140729021044) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "campaigns", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "meets_at"
    t.string   "meets_when"
    t.integer  "status"
  end

  create_table "character_feats", force: true do |t|
    t.string  "name"
    t.text    "benefit"
    t.text    "notes"
    t.integer "character_id"
  end

  create_table "character_items", force: true do |t|
    t.string  "name"
    t.text    "description"
    t.integer "character_id"
    t.integer "quantity",     default: 0
  end

  create_table "character_skills", force: true do |t|
    t.string  "name"
    t.integer "points"
    t.integer "character_id"
    t.text    "notes"
  end

  create_table "characters", force: true do |t|
    t.string   "name"
    t.text     "bio"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "race"
    t.string   "char_class"
    t.string   "alignment"
    t.string   "gender"
    t.integer  "total_str"
    t.integer  "total_dex"
    t.integer  "total_con"
    t.integer  "total_int"
    t.integer  "total_wis"
    t.integer  "total_cha"
    t.integer  "campaign_id"
    t.integer  "level",       default: 1
    t.integer  "experience",  default: 0
    t.integer  "money",       default: 0
  end

  create_table "users", force: true do |t|
    t.string "email"
    t.string "password_digest"
    t.string "user_name"
    t.text   "about"
  end

end
