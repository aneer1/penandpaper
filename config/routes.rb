Rails.application.routes.draw do
  namespace :players do
    resource :pages, path:'/'
    resources :characters do
      scope module: :characters do
        resource :leaves, path: 'leave', path_names: { leave:'' }
        resources :items
        resources :skills
        resources :feats
      end
    end
    resources :campaigns do
      scope module: :campaigns do
        resource :joins, path: 'join', path_names: { new: '' }
      end
    end
  end

  namespace :game_masters, path: 'gm' do
    resources :campaigns do
      scope module: :campaigns do
        resources :characters do
          scope module: :characters do
            resources :items
          end
        end
      end
    end
  end

  namespace :users, path: 'user' do
    resource :sign_ups, path: 'sign-up'
    resource :profiles, path: 'profile'
  end

  resources :users
  resources :characters
  resource :sessions, path: 'session'
  resources :campaigns
  resources :pages, path: '/'
end
